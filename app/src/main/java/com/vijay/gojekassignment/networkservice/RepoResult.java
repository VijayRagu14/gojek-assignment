package com.vijay.gojekassignment.networkservice;

import androidx.lifecycle.LiveData;

import com.vijay.gojekassignment.model.RepositoryModel;

import java.util.List;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class RepoResult {
    private LiveData<List<RepositoryModel>> data;
    private LiveData<String> networkErrors;

    public RepoResult(LiveData<List<RepositoryModel>> data, LiveData<String> networkErrors) {
        this.data = data;
        this.networkErrors = networkErrors;
    }

    public LiveData<List<RepositoryModel>> getData() {
        return data;
    }

    public void setData(LiveData<List<RepositoryModel>> data) {
        this.data = data;
    }

    public LiveData<String> getNetworkErrors() {
        return networkErrors;
    }

    public void setNetworkErrors(LiveData<String> networkErrors) {
        this.networkErrors = networkErrors;
    }
}
