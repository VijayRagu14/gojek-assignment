package com.vijay.gojekassignment.networkservice;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vijay.gojekassignment.AppExecutor;
import com.vijay.gojekassignment.appcache.RepoLocalCache;
import com.vijay.gojekassignment.model.RepositoryModel;

import java.util.List;


/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class RepositoryService {

    private static final int NETWORK_PAGE_SIZE = 1;
    private AppNetworkService.AppNetworkServiceCall service;
    private RepoLocalCache cache;
    // keep the last requested page. When the request is successful, increment the page number.
    private int lastRequestedPage = 1;
    // LiveData of network errors.
    private MutableLiveData<String> networkErrors = new MutableLiveData<>();
    // avoid triggering multiple requests in the same time
    private boolean isRequestInProgress = false;

    public RepositoryService(AppNetworkService.AppNetworkServiceCall service, RepoLocalCache cache) {
        this.service = service;
        this.cache = cache;
    }

    private void requestAndSaveData() {
        if (isRequestInProgress)
            return;

        isRequestInProgress = true;

        AppNetworkService.searchRepos(service, lastRequestedPage, NETWORK_PAGE_SIZE, new AppNetworkService.SearchListener() {
            @Override
            public void onSuccess(List<RepositoryModel> repos) {
                cache.insert(repos, new AppExecutor() {
                    @Override
                    public void executeThis() {
                        lastRequestedPage++;
                        isRequestInProgress = false;
                    }
                });
            }

            @Override
            public void onError(String error) {
                networkErrors.postValue(error);
                isRequestInProgress = false;
            }
        });
    }


    public void requestMore() {
        requestAndSaveData();
    }

    public RepoResult search() {
        lastRequestedPage = 1;
        requestAndSaveData();

        // Get data from the local cache
        LiveData<List<RepositoryModel>> data = cache.reposByName();

        return new RepoResult(data, networkErrors);
    }
}
