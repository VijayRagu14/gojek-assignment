package com.vijay.gojekassignment.networkservice;

import android.util.Log;

import com.vijay.gojekassignment.model.RepositoryModel;
import com.vijay.gojekassignment.model.RepositoryResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class AppNetworkService {
    private static final String TAG = "AppNetworkService";
    private static final String IN_QUALIFIER = "language=&since=daily";
    private static final String BASE_URL = "https://github-trending-api.now.sh/";

    public static AppNetworkServiceCall create() {
        final HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logger)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AppNetworkServiceCall.class);
    }

    public static void searchRepos1(AppNetworkServiceCall service, String query, int page, int itemsPerPage, final SearchListener iSearchListener) {
        Log.d(TAG, "query: " + query + ", page: " + page + ", itemsPerPage: " + itemsPerPage);

        String apiQuery = query + IN_QUALIFIER;

        service.searchRepos().enqueue(new Callback<List<RepositoryModel>>() {
            @Override
            public void onResponse(Call<List<RepositoryModel>> call, Response<List<RepositoryModel>> response) {
                Log.d(TAG, "got a response " + response);

                if (response.isSuccessful()) {
//                    List<RepositoryModel> repos = new ArrayList<>();
                    List<RepositoryResponse> repoResponse = new ArrayList<>();
                    List<RepositoryModel> repos = response.body();
//                    if (repos != null)
//                        repoResponse = null;
                    iSearchListener.onSuccess(repos);

                } else {
                    ResponseBody errorBody = response.errorBody();

                    try {
                        iSearchListener.onError(errorBody != null ? errorBody.string() : "Unknown error");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RepositoryModel>> call, Throwable t) {
                Log.d(TAG, "fail to get data");
                iSearchListener.onError(t.getMessage() != null ? t.getMessage() : "unknown error");
            }
        });

    }

    public static void searchRepos(AppNetworkServiceCall service, int page, int itemsPerPage, final SearchListener iSearchListener) {
        Log.d(TAG, " page: " + page + ", itemsPerPage: " + itemsPerPage);

        String apiQuery = IN_QUALIFIER;

        service.searchRepos(page, itemsPerPage).enqueue(new Callback<List<RepositoryModel>>() {
            @Override
            public void onResponse(Call<List<RepositoryModel>> call, Response<List<RepositoryModel>> response) {
                Log.d(TAG, "got a response " + response);

                if (response.isSuccessful()) {
                    List<RepositoryModel> repos = response.body();
                    iSearchListener.onSuccess(repos);
                } else {
                    ResponseBody errorBody = response.errorBody();

                    try {
                        iSearchListener.onError(errorBody != null ? errorBody.string() : "Unknown error");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RepositoryModel>> call, Throwable t) {
                Log.d(TAG, "fail to get data");
                iSearchListener.onError(t.getMessage() != null ? t.getMessage() : "unknown error");
            }
        });

    }

    public interface AppNetworkServiceCall {

        @GET("repositories?")
        Call<List<RepositoryModel>> searchRepos(@Query("q") String query,
                                                @Query("page") int page,
                                                @Query("per_page") int itemsPerPage);

        @GET("repositories?")
        Call<List<RepositoryModel>> searchRepos(@Query("page") int page,
                                                @Query("per_page") int itemsPerPage);


        @GET("repositories?")
        Call<List<RepositoryModel>> searchRepos();
    }

    public interface SearchListener {
        void onSuccess(List<RepositoryModel> repos);

        void onError(String error);
    }
}
