package com.vijay.gojekassignment;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */

public abstract class AppExecutor {
    public abstract void executeThis();
}
