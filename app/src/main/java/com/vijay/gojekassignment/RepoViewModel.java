package com.vijay.gojekassignment;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.vijay.gojekassignment.model.RepositoryModel;
import com.vijay.gojekassignment.networkservice.RepoResult;
import com.vijay.gojekassignment.networkservice.RepositoryService;

import java.util.List;

/**
 * Created by vijay on 2019-11-17.
 * vg4029@gmail.com
 */
public class RepoViewModel extends ViewModel {

    private static final int VISIBLE_THRESHOLD = 5;
    private RepositoryService repository;
    private MutableLiveData<String> queryLiveData = new MutableLiveData<String>();
    public LiveData<RepoResult> repoResult = Transformations.map(queryLiveData, new Function<String, RepoResult>() {
        @Override
        public RepoResult apply(String input) {
            return repository.search();
        }
    });

    public RepoViewModel(RepositoryService repository) {
        this.repository = repository;
    }

    public LiveData<List<RepositoryModel>> repos = Transformations.switchMap(repoResult, new Function<RepoResult, LiveData<List<RepositoryModel>>>() {
        @Override
        public LiveData<List<RepositoryModel>> apply(RepoResult input) {
            return input.getData();
        }
    });

    public LiveData<String> networkErrors = Transformations.switchMap(repoResult, new Function<RepoResult, LiveData<String>>() {
        @Override
        public LiveData<String> apply(RepoResult input) {
            return input.getNetworkErrors();
        }
    });

    public void listScrolled(int visibleItemCount, int lastVisibleItemPosition, int totalItemCount) {
        if (visibleItemCount + lastVisibleItemPosition + VISIBLE_THRESHOLD >= totalItemCount) {
            String immutableQuery = lastValue();
//            if (immutableQuery != null) {
            repository.requestMore();
//            }
        }
    }

    public void searchRepo(String queryString) {
        queryLiveData.postValue(queryString);
    }


    public String lastValue() {
        return queryLiveData.getValue();
    }
}