package com.vijay.gojekassignment.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class BuiltByConvertor {


    @TypeConverter
    public static List<BuiltByModel> fromJson(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<BuiltByModel>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(data, listType);
    }


    @TypeConverter
    public static String fromArrayList(List<BuiltByModel> builtByModels) {
        Gson gson = new Gson();
        String json = gson.toJson(builtByModels);
        return json;
    }
}
