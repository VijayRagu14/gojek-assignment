package com.vijay.gojekassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class BuiltByModel {

    @SerializedName("href")
    public String href;

    @SerializedName("avatar")
    public String avatar;

    @SerializedName("username")
    public String username;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
