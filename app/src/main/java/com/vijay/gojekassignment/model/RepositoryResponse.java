package com.vijay.gojekassignment.model;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class RepositoryResponse {

//    private List<RepositoryModel> items = new ArrayList<>();
//
//    public List<RepositoryModel> getItems() {
//        return items;
//    }
//
//    public void setItems(List<RepositoryModel> items) {
//        this.items = items;
//    }

    private RepositoryModel items;

    public RepositoryModel getItems() {
        return items;
    }

    public void setItems(RepositoryModel items) {
        this.items = items;
    }
}
