package com.vijay.gojekassignment.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
@Entity(tableName = "repos")
public class RepositoryModel {

    @SerializedName("builtBy")
    @TypeConverters({BuiltByConvertor.class})
    public List<BuiltByModel> builtByModelList;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo
    int id;

    @SerializedName("author")
    @ColumnInfo
    String author;

    @SerializedName("name")
    @ColumnInfo
    String name;

    @SerializedName("avatar")
    @ColumnInfo
    String avatar;

    @SerializedName("url")
    @ColumnInfo
    String url;

    @SerializedName("description")
    @ColumnInfo
    String description;

    @SerializedName("language")
    @ColumnInfo
    String language;

    @SerializedName("languageColor")
    @ColumnInfo
    String languageColor;

    @SerializedName("stars")
    @ColumnInfo
    String stars;

    @SerializedName("forks")
    @ColumnInfo
    String forks;

    @SerializedName("currentPeriodStars")
    @ColumnInfo
    String currentPeriodStars;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageColor() {
        return languageColor;
    }

    public void setLanguageColor(String languageColor) {
        this.languageColor = languageColor;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getForks() {
        return forks;
    }

    public void setForks(String forks) {
        this.forks = forks;
    }

    public String getCurrentPeriodStars() {
        return currentPeriodStars;
    }

    public void setCurrentPeriodStars(String currentPeriodStars) {
        this.currentPeriodStars = currentPeriodStars;
    }

    public List<BuiltByModel> getBuiltByModelList() {
        return builtByModelList;
    }

    public void setBuiltByModelList(List<BuiltByModel> builtByModelList) {
        this.builtByModelList = builtByModelList;
    }
}
