package com.vijay.gojekassignment;

import android.content.Context;

import androidx.lifecycle.ViewModelProvider;

import com.vijay.gojekassignment.appcache.RepoLocalCache;
import com.vijay.gojekassignment.database.GojekDB;
import com.vijay.gojekassignment.networkservice.AppNetworkService;
import com.vijay.gojekassignment.networkservice.RepositoryService;

import java.util.concurrent.Executors;

/**
 * Created by vijay on 2019-11-17.
 * vg4029@gmail.com
 */
public class Injection {

    private static RepoLocalCache provideCache(Context context) {
        GojekDB database = GojekDB.getInstance(context);
        return new RepoLocalCache(database.reposDao(), Executors.newSingleThreadExecutor());
    }

    private static RepositoryService provideGithubRepository(Context context) {
        return new RepositoryService(AppNetworkService.create(), provideCache(context));
    }

    public static ViewModelProvider.Factory provideViewModelFactory(Context context) {
        return new ViewModelFactory(provideGithubRepository(context));
    }
}
