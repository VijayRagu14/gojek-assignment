package com.vijay.gojekassignment.appcache;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.vijay.gojekassignment.AppExecutor;
import com.vijay.gojekassignment.database.RepoDao;
import com.vijay.gojekassignment.model.RepositoryModel;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
public class RepoLocalCache {
    private RepoDao repoDao;
    private Executor ioExecutor;

    public RepoLocalCache(RepoDao repoDao, Executor executor) {
        this.repoDao = repoDao;
        this.ioExecutor = executor;
    }

    public void insert(final List<RepositoryModel> repos, final AppExecutor iBlockExecutor) {
        ioExecutor.execute(new Runnable() {
            @Override
            public void run() {
                Log.d("LocalCache", "inserting " + repos.size() + " repos");
                repoDao.insert(repos);
                iBlockExecutor.executeThis();
            }
        });
    }

    public LiveData<List<RepositoryModel>> reposByName() {
//        name = name.replaceAll(" ", "%");
//        String query = "%" + name + "%";
//        return repoDao.getRepoData(query);
        return repoDao.getAllRepoData();
    }
}
