package com.vijay.gojekassignment.ui;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.vijay.gojekassignment.model.RepositoryModel;

/**
 * Created by vijay on 2019-11-17.
 * vg4029@gmail.com
 */
public class RepoAdapter extends ListAdapter<RepositoryModel, RecyclerView.ViewHolder> {

    private static final DiffUtil.ItemCallback<RepositoryModel> REPO_COMPARATOR = new DiffUtil.ItemCallback<RepositoryModel>() {
        @Override
        public boolean areItemsTheSame(RepositoryModel oldItem, RepositoryModel newItem) {
            String oldFullName = oldItem.getName() != null ? oldItem.getName() : "";
            String newFullName = newItem.getName() != null ? newItem.getName() : "";
            Log.e("oldFullName", oldFullName);
            Log.e("newFullName", newFullName);
            return oldFullName.equals(newFullName);
        }

        @Override
        public boolean areContentsTheSame(RepositoryModel oldItem, RepositoryModel newItem) {
            return oldItem.equals(newItem);
        }
    };
    private int mExpandedPosition = -1;

    public RepoAdapter() {
        super(REPO_COMPARATOR);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return RepoViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        RepositoryModel repoItem = getItem(position);
        if (repoItem != null) {
            RepoViewHolder repoViewHolder = (RepoViewHolder) holder;
            repoViewHolder.bind(repoItem);

            final boolean isExpanded = position == mExpandedPosition;
            repoViewHolder.lang_star_fork.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            repoViewHolder.item_body.setActivated(isExpanded);
            repoViewHolder.item_body.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mExpandedPosition = isExpanded ? -1 : position;
                    notifyItemChanged(position);
                }
            });
        }
    }
}
