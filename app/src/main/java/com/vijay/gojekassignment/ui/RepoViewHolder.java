package com.vijay.gojekassignment.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.vijay.gojekassignment.R;
import com.vijay.gojekassignment.customview.CircleTransform;
import com.vijay.gojekassignment.model.RepositoryModel;

/**
 * Created by vijay on 2019-11-17.
 * vg4029@gmail.com
 */
public class RepoViewHolder extends RecyclerView.ViewHolder {


    public static Context mContext;
    public RelativeLayout item_body;
    public LinearLayout lang_star_fork;
    public ImageView profilePic;
    private TextView repoName;
    private TextView description;
    private TextView stars;
    private TextView language;
    private TextView forks;

    public RepoViewHolder(View itemView) {
        super(itemView);
        profilePic = itemView.findViewById(R.id.profile_pic_civ);
        repoName = itemView.findViewById(R.id.repo_name_tv);
        description = itemView.findViewById(R.id.repo_description_tv);
        language = itemView.findViewById(R.id.repo_language_iv);
        stars = itemView.findViewById(R.id.repo_stars_iv);
        forks = itemView.findViewById(R.id.repo_forks_iv);
        item_body = itemView.findViewById(R.id.item_body_rl);
        lang_star_fork = itemView.findViewById(R.id.lang_star_fork_ll);
    }

    public static RepoViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_view_item, parent, false);
        mContext = parent.getContext();
        return new RepoViewHolder(view);
    }

    void bind(@Nullable RepositoryModel repoModel) {
        if (repoModel == null) {
            Resources resources = itemView.getResources();

            description.setVisibility(View.GONE);
            language.setVisibility(View.GONE);
            stars.setText(resources.getString(R.string.unknown));
            forks.setText(resources.getString(R.string.unknown));
        } else {
            showRepoData(repoModel);
        }
    }

    private void showRepoData(RepositoryModel repoModel) {
        int nameVisibility = View.GONE;
        if (repoModel.getDescription() != null) {
            repoName.setText(repoModel.getName());
            nameVisibility = View.VISIBLE;
        }
        repoName.setVisibility(nameVisibility);

        int descriptionVisibility = View.GONE;
        if (repoModel.getDescription() != null) {
            description.setText(repoModel.getDescription());
            descriptionVisibility = View.VISIBLE;
        }
        description.setVisibility(descriptionVisibility);

        stars.setText(String.valueOf(repoModel.getStars()));
        forks.setText(String.valueOf(repoModel.getForks()));

        int languageVisibility = View.GONE;
        if (!TextUtils.isEmpty(repoModel.getLanguage())) {
            Resources resources = this.itemView.getResources();
            language.setText(repoModel.getLanguage());
            languageVisibility = View.VISIBLE;
        }
        language.setVisibility(languageVisibility);

        Glide.with(mContext)
                .load(repoModel.getAvatar())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Drawable d = mContext.getResources().getDrawable(R.drawable.ic_person_black_24dp);
                        d.setBounds(0, 0, 80, 80);
                        profilePic.setImageDrawable(d);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        return false;
                    }
                })
                .transform(new CircleTransform(mContext))
                .placeholder(R.drawable.ic_person_black_24dp)
                .into(profilePic);
    }

}
