package com.vijay.gojekassignment.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.vijay.gojekassignment.Injection;
import com.vijay.gojekassignment.R;
import com.vijay.gojekassignment.RepoViewModel;
import com.vijay.gojekassignment.model.RepositoryModel;

import java.util.List;

public class RepoHome extends AppCompatActivity {

    private static final String LAST_SEARCH_QUERY = "last_search_query";
    private static final String DEFAULT_QUERY = "Android";
    LinearLayoutManager layoutManager;
    private RepoViewModel viewModel;
    private RepoAdapter adapter = new RepoAdapter();
    private RecyclerView mListRV;
    private TextView mEmptyList;
    private ShimmerFrameLayout mShimmerViewContainer;
    private SwipeRefreshLayout mRefreshSRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initOperation(savedInstanceState);
        setupScrollListener();
        initAdapter();
    }

    private void initView() {
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mRefreshSRL = findViewById(R.id.refresh_srl);
        mListRV = findViewById(R.id.list_rv);
        mEmptyList = findViewById(R.id.empty_list_tv);
    }

    private void initOperation(Bundle savedInstanceState) {
        // get the view model
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
                .get(RepoViewModel.class);

        // add dividers between RecyclerView's row items
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        mListRV.addItemDecoration(decoration);

        String query = savedInstanceState != null ? (savedInstanceState.getString(LAST_SEARCH_QUERY) != null
                ? savedInstanceState.getString(LAST_SEARCH_QUERY) : DEFAULT_QUERY) : DEFAULT_QUERY;
        viewModel.searchRepo(query);

        mRefreshSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                int totalItemCount = layoutManager.getItemCount();
                int visibleItemCount = layoutManager.getChildCount();
                int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount);
            }
        });
    }

    private void setupScrollListener() {
        layoutManager = (LinearLayoutManager) mListRV.getLayoutManager();

        mListRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = layoutManager.getItemCount();
                int visibleItemCount = layoutManager.getChildCount();
                int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount);
            }
        });

    }

    private void initAdapter() {
        mListRV.setAdapter(adapter);

        viewModel.repos.observe(this, new Observer<List<RepositoryModel>>() {
            @Override
            public void onChanged(@Nullable List<RepositoryModel> repos) {
                int size = repos.size();
                showEmptyList(size == 0);
                adapter.submitList(repos);
            }
        });

        viewModel.networkErrors.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(RepoHome.this, "\uD83D\uDE28 Wooops ${it}", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showEmptyList(boolean show) {
        if (show) {
            mEmptyList.setVisibility(View.GONE);
            mListRV.setVisibility(View.GONE);
            mShimmerViewContainer.setVisibility(View.VISIBLE);
        } else {
            mEmptyList.setVisibility(View.GONE);
            mListRV.setVisibility(View.VISIBLE);
            mShimmerViewContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LAST_SEARCH_QUERY, viewModel.lastValue());
    }
}
