package com.vijay.gojekassignment;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.vijay.gojekassignment.networkservice.RepositoryService;

/**
 * Created by vijay on 2019-11-17.
 * vg4029@gmail.com
 */
public class ViewModelFactory implements ViewModelProvider.Factory {

    private RepositoryService repository;

    public ViewModelFactory(RepositoryService repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(RepoViewModel.class)) {
            return (T) new RepoViewModel(repository);
        }

        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}

