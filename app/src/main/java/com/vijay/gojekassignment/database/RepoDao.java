package com.vijay.gojekassignment.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.vijay.gojekassignment.model.RepositoryModel;

import java.util.List;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
@Dao
public interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<RepositoryModel> posts);


    @Query("SELECT * FROM repos WHERE (name LIKE :query) OR (description LIKE :query) ORDER BY currentPeriodStars DESC, name ASC")
    LiveData<List<RepositoryModel>> getRepoData(String query);

    @Query("SELECT * FROM repos ORDER BY name ASC")
    LiveData<List<RepositoryModel>> getAllRepoData();
}
