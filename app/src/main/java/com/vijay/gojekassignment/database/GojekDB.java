package com.vijay.gojekassignment.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.vijay.gojekassignment.model.RepositoryModel;

/**
 * Created by vijay on 2019-11-16.
 * vg4029@gmail.com
 */
@Database(entities = {RepositoryModel.class}, version = 1, exportSchema = false)
public abstract class GojekDB extends RoomDatabase {

    public static GojekDB getInstance(Context iContext) {
        return Room.databaseBuilder(iContext,
                GojekDB.class, "Github.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    abstract public RepoDao reposDao();
}
